package com.example.gautamactivity;

import java.io.FileWriter;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * @author GAUTAM B
 * Version 1
 */
public class MainActivity extends Activity implements OnClickListener {
	FileWriter f;
	/**
	 * OnResume of MainActivity
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			f.append("On Resume!");
			Toast.makeText(this, "On Resume Screen1", Toast.LENGTH_SHORT)
					.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * OnStart of MainActivity
	 */
	@Override
	protected void onStart() {
		try {
			f.append("On Start!");
			Toast.makeText(this, "On Start Screen1", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onStart();
	}
	/**
	 * OnStop of MainActivity
	 */
	@Override
	protected void onStop() {
		try {
			f.append("On Stop!");
			Toast.makeText(this, "On Stop Screen1", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onStop();
	}
	/**
	 * OnPause of MainActivity
	 */
	@Override
	protected void onPause() {
		try {
			f.append("On Pause!");
			Toast.makeText(this, "On Pause Screen1", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onPause();
	}
	/**
	 * OnDestroy of MainActivity
	 */
	@Override
	protected void onDestroy() {
		try {
			f.flush();
			f.close();
			Toast.makeText(this, "On Destroy Screen1", Toast.LENGTH_SHORT)
					.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO Auto-generated method stub
		super.onDestroy();
	}
	/**
	 * OnCreate of MainActivity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		try {
			f = new FileWriter(
					"/data/data/com.example.gautamactivity/alert.txt");

		} catch (Exception ee) {
			Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toast.makeText(this, "On Create Screen1", Toast.LENGTH_SHORT).show();
		Button b1 = (Button) findViewById(R.id.button1);
		b1.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	/**
	 * Click of the Button
	 */
	@Override
	public void onClick(View v) {
		Intent i = new Intent(MainActivity.this, Screen2Activity.class);
		startActivity(i);

	}

}

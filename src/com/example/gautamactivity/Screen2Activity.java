package com.example.gautamactivity;

import java.io.FileWriter;
import java.io.IOException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * @author Gautam B 
 *  Version 1
 */
public class Screen2Activity extends Activity implements OnClickListener {
	Button mScreen2Button;
	FileWriter f;

	/**
	 * OnResume of Screen2 Activity
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			f.append("On Resume!");
			Toast.makeText(this, "OnResume Screen2", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * OnResume of Screen2 Activity
	 */
	@Override
	protected void onStart() {
		try {
			f.append("On Start!");
			Toast.makeText(this, "On Start Screen2", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onStart();
	}

	/**
	 * OnStop of Screen2 Activity
	 */
	@Override
	protected void onStop() {
		try {
			f.append("On Stop!");
			Toast.makeText(this, "on Stop Screen2", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onStop();
	}

	/**
	 * OnPause of Screen2 Activity
	 */
	@Override
	protected void onPause() {
		try {
			f.append("On Pause!");
			Toast.makeText(this, "On Pause Screen2", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onPause();
	}

	/**
	 * OnDestroy of Screen2 Activity
	 */
	@Override
	protected void onDestroy() {
		try {
			f.flush();
			f.close();
			Toast.makeText(this, "On Destroy Screen2", Toast.LENGTH_SHORT)
					.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO Auto-generated method stub
		super.onDestroy();
	}

	/**
	 * OnCreate of Screen2 Activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen2);
		mScreen2Button = (Button) findViewById(R.id.button1);
		mScreen2Button.setOnClickListener(this);

		try {
			f = new FileWriter(
					"/data/data/com.example.gautamactivity/alert.txt");

		} catch (Exception ee) {
			Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();
		}
		Toast.makeText(this, "On Crate Screen2", Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.screen2, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		Intent i = new Intent(Screen2Activity.this, MainActivity.class);
		startActivity(i);
	}

}
